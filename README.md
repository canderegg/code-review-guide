# Alarm.com Code Review Style Guide


This document contains guidelines for producing clean, maintainable C# code.

I realize there is a lot of information here. Don't feel the need to apply all 
of this at once. Doing code reviews is a skill just like anything else. **If you 
take nothing else away from this guide, go read the [Must Fix](#must-fix) 
section.**

Pick a couple of things from the [Must Fix](#must-fix) category and focus on 
those. Just getting everybody on the same page with those issues would be a 
huge step forward. As you get better at watching out for those things (and the 
people you're reviewing make fewer of those mistakes) you can come back here 
and pick some new things to look for. Work your way down the list.

## Table of Contents

* [Code Review Philosophy](#code-review-philosophy)
* [Must Fix](#must-fix)
  * [Actual Defects](#actual-defects)
  * [Massive Anything](#massive-anything)
  * [Lack of Unit Tests](#lack-of-unit-tests)
* [Will Fix](#will-fix)
  * [Obsolete Anything](#obsolete-anything)
  * [Building SQL in Code](#building-sql-in-code)
  * [Poor Commenting](#poor-commenting)
  * [Misleading Variables and Methods](#misleading-variables-and-methods)
  * [Switch Statements](#switch-statements)
* [Should Fix](#should-fix)
  * [Duplicate Code](#duplicate-code)
  * [Over-Generalization](#over-generalization)
  * [Data Classes and Extension Classes](#data-classes-and-extension-classes)
  * [Shotgun Surgery](#shotgun-surgery)
  * [Refused Bequests](#refused-bequests)

## Code Review Philosophy

ADC has a relatively fast build cycle that takes all changes to production 
within week or two of when they're checked in. We also have a high proportion 
of new engineers relative to senior engineers. This means people are doing a 
lot of code reviews in short order without much (or any) guidance on what to 
look for. This document should provide some guidance and uniformity to our code 
review process.

Here are some things to keep in mind:

* **Code reviews take time.** It can take a couple days to properly review a big 
  set of changes. When you're checking stuff in, consider your timing. Give your 
  code reviewer some lead time for big tickets. Your changes should be on a feature
  branch so they don't get merged until after code review, but make sure you consider
  review time as you're planning deadlines.
* **Code reviews are collaborative.** A good code review should have a lot of 
  comments and discussion. This is good. It can be frustrating when you're the 
  dev and under a tight deadline, but at the end of the day having more people 
  thinking about the code makes it better. As a code reviewer, you shouldn't 
  just be a rubber stamp.
* **Getting a ticket sent back from code review isn't bad.** Getting a ticket 
  back means you get to improve your code. Maybe it's just a stupid mistake that 
  got caught before it caused problems in production. Maybe we have a utility 
  method you never knew about. Maybe there's a more efficient way of doing 
  things. Sending a ticket back isn't a reflection on the dev, as the reviewer 
  you're helping them write cleaner, tighter code.
* **Tickets can come back from code review for reasons unrelated to 
  functionality.** Code reviews are an opportunity to enforce good code style. 
  Code that makes it through review becomes part of our codebase indefinitely. 
  Don't pass code you wouldn't want to work on in the future.

## Must Fix

> Things in the Must Fix category are breaking. Code that includes these issues 
should not be passed through code review. The ticket should be sent back to the 
dev for a fix in the current build cycle.

### Actual Defects

This is mostly what reviewers are looking for now: logical flaws, SQL or XSS 
injection vulnerabilities, missing translations, etc. These are issues with the 
functionality of the ticket. Obviously we want to keep looking for these things 
in code review; the ticket should be sent back to dev for a fix.

**Example:**
```c#
// This is logically dead code.
if (condition && !condition)
{
    return "Impossible!";
}
  
// This is a SQL injection security risk.
string sql = string.Format(
    "update tbl_customers set firstname='{0}' where customer_id={1}"
    , nameStringFromCustomer
    , unit.Customer.CustomerId);
DBHelper.ExecuteNonQuery(sql);
```

### Massive Anything

12,000 line classes, 300 line methods, 120 case switch statements - our codebase 
has all of them. Monolithic code on this scale is indicative of a serious 
problem with the architecture. It's hard to understand, hard to review, and 
brittle to future changes.

I won't go so far as to say devs should be refactoring existing monolithic code 
when they touch it, but we should certainly not be getting any new code like 
this checked in. If you're reviewing some massive edifice of code, work with the 
dev to restructure it into more manageable pieces.

**Example:**
```c#
// This is horrifying, we should instead be using classes and polymorphism.
switch (dealer.DealerId)
{
    case 1:
        return "Alarm.com";
    case 2:
        return "Interlogix";
    case 3:
        return "Vector Security";
  
    // ... 1000 cases later
  
    case 1115:
        return "Alarm.com Dev Dealer";
}
```

### Lack of Unit Tests

Unit tests are part of your development work. In addition to providing you 
assurance that your code is correct when checked in, they help you design for 
good encapsulation and they provide a safety net for anyone modifying your code 
in the future.

All new code added to our codebase should have substantial unit test coverage. 
If new code lacks unit tests, there should be a discussion about why that is.
* Is this code too tightly coupled to existing classes? 
* Is there something in the existing architecture that makes this hard to test?
* Are there ways the code can be refactored to enable easier testing?

Unless there are sound reasons why new code cannot be unit tested, it should be 
sent back to dev until satisfactory tests are added.

**Example:**
```c#
// Really? Just look at our codebase.
```

## Will Fix

> Things in the Will Fix category are serious but not breaking. If the dev 
cannot fix the issues in the current build cycle, a follow-up ticket should be 
created to fix them in the next build cycle.

### Obsolete Anything

If the ticket removed references to something, it may have generated an obsolete 
thing! Obsolete things include unused classes and methods, commented-out code, 
and old versions of stored procedures. If it's not in use, it shouldn't be in 
our codebase. Don't worry about needing to go back and recover it later, that's 
why we have version control.

If possible, this should be removed as part of the original ticket. If for some 
reason that cannot be done, a follow-up ticket should be submitted in the next 
build cycle to clean these things up.

> This is particularly important with stored procedures. We should basically 
never have multiple versions of the same stored procedure in the database at 
once. Unless one explicitly calls the other, they get super hard to maintain in 
parallel. If one does call the other but they do subtly different things, they 
should have different names to reflect this.

**Example:**
```c#
// Code before change.
DataSet ds = DBHelper.GetDataset("spa_GetCustomerInformation", customerId);
  
// Code after change.
DataSet ds = DBHelper.GetDataset("spa_GetCustomerInformation_b", customerId, dealerId);
 
// The procedure spa_GetCustomerInformation has been made obsolete by spa_GetCustomerInformation_b,
// it should be removed as part of this ticket.
```

### Building SQL in Code

If you're making a single select query, with known structure and parameters, you 
can build it as a string in code. If you're doing more than that (multiple 
related selects, related selects and updates, updates to multiple tables, etc.) 
you should probably be using a stored procedure instead. Round-trip time to the 
database is nontrivial; it's substantially more efficient to do things in a 
single procedure call.

Code that doesn't follow this guideline should be sent back to dev to get 
refactored into a proc.

>As a general rule, if you're building and running more than one SQL query 
string in a single method, you should either be using a stored procedure or 
splitting your method up into smaller submethods.

**Example:**
```c#
// This is acceptable.
string sql = string.Format(
    "select firstname, lastname from lu_customer where customer_id={0}"
    , customerId);
DataSet ds = DBHelper.GetDataset(sql);
  
  
// This is bad, and should be done with a stored procedure.
string sql = string.Format(
    "select top 1 customer_id from lu_customer where firstname='{0}' and lastname='{1}' {2}"
    , customer.FirstName
    , customer.LastName
    , limitToDealer ? string.Format("and dealer_id={0}", customer.Dealer.DealerId) : "");
DataSet ds = DBHelper.GetDataset(sql);
  
if (ds.IsEmpty())
{
    sql = string.Format(
        "insert into lu_customer (firstname, lastname) values ('{0}', '{1}')"
        , DBHelper.ConvertDbSafe(newCustomerFirstName)
        , DBHelper.ConvertDbSafe(newCustomerLastName));
}
else
{
    sql = string.Format(
        "update lu_customer set firstname='{0}', lastname='{1}' where customer_id={2}"
        , DBHelper.ConvertDbSafe(newCustomerFirstName)
        , DBHelper.ConvertDbSafe(newCustomerLastName)
        , Convert.ToInt32(ds.Tables[0].Rows[0]["customer_id"]));
}
DBHelper.ExecuteNonQuery(sql);
```

### Poor Commenting
Every new class and method that gets checked in should include XML doc comments. 
In addition to providing useful information to other developers, they are 
machine readable and can be used by the IDE (for example to provide usage 
tooltips like the one below, or auto-generate class documentation).

![automatically generated tooltip](tooltip.png)

In addition to XML doc comments, any areas of the code that are not self-evident 
should have supporting comments. If the code reviewer needs to ask for 
clarification on anything, the code should probably have better commenting.

Obsolete comments that are no longer accurate are worse than no comments at all. 
Whenever a method changes, the comments relating to it should be checked to 
verify that they are still accurate.

**Example:**
```c#
// This is awesome. It can seem like overkill (especially on simple methods) but it makes
// things super simple for future devs working with your code.
  
/// <summary>
/// Generates a greeting string customized to the given name.
/// </summary>
/// <param name="name"> The user's name to use when building the greeting. </param>
/// <returns> A greeting string customized for the user. </returns>
public string GetGreetingString(string name)
{
    return $"Hello {name}";
}
  
  
// Oh no! Somebody updated the method but not the comments. Now everything is confusing.
  
/// <summary>
/// Generates a greeting string customized to the given name.
/// </summary>
/// <param name="name"> The user's name to use when building the greeting. </param>
/// <returns> A greeting string customized for the user. </returns>
public string GetGreetingString(int age)
{
    return $"Hello user! You are {age} years old.";
}
```

### Misleading Variables and Methods

This includes names that don't make it clear what's in the variable or what the 
method does, names that describe old code behaviors that are no longer true, and 
names that are very similar to each other. This is confusing for the dev, the 
reviewer, and anybody looking at the code in the future. It should be fixed as 
soon as possible.

**Example:**
```c#
// These names are semantically meaningless and should be avoided.
int x = 5;
string str = "Hello";
  
// These names describe old behavior that is no longer true.
string userName = "Not the user name";
  
private DateTime GetCustomerJoinDate(Customer customer)
{
    // return customer.JoinDate;
    return customer.TermDate;
}
  
// These names are too similar and likely to be filled in wrong when tab completing.
private string GetCustomerPhoneNumber(Customer customer)
{
    return customer.PhoneNumbers.First();
}
  
private string GetCustomerPhoneNumbers(Customer customer)
{
    return string.Join(", ", customer.PhoneNumbers);
}
```

### Switch Statements

Switch Statements (particularly when paired with Enums) are an antipattern. They 
indicate the current architecture is poorly designed to leverage the power of 
OOP. Any time you see a switch statement you should be thinking about how to 
accomplish that same task with objects and polymorphism instead.

The reason Switch Statements are categorized as "Will Fix" rather than "Should 
Fix" is that they show up everywhere in our codebase. People learn by reading 
the existing code, and this is one thing we don't want them to learn. Switch 
Statements tend to lead to other antipatterns (see 
[Data Classes](#data-classes-and-extension-classes) and 
[Shotgun Surgery](#shotgun-surgery) below).

**Example:**
```c#
// An enum for thermostat type.
public enum ThermostatType
{
    ZWaveThermostat = 1,
    ZigbeeThermostat = 2,
    CloudThermostat = 3
};
  
// A class to hold fields and methods related to thermostats.
public class Thermostat
{
    // The type of this thermostat.
    public ThermostatType Type { get; set; }
  
    // Handle sending a command to this thermostat.
    public void SendSetpointCommand(decimal setpointF) {
        // Switch on an enum, taking different actions for different possible values.
        switch (this.Type)
        {
            case ThermostatType.ZWaveThermosat:
                SendZWaveSetpointCommand(setpointF);
                break;
            case ThermostatType.ZigbeeThermosat:
                SendZigbeeSetpointCommand(setpointF);
                break;
            case ThermostatType.CloudThermosat:
                SendCloudSetpointCommand(setpointF);
                break;
        }
    }
  
    // More methods that switch to determine behavior.
}
  
// Wait, this is terrible!
// We should be using inheritance here. We can have a base class for thermostats, and then extend that
// base class for each distinct type of thermostat. We can then override the SendSetpointCommand method
// in each subclass to use a different implementation. That's so much cleaner.
```

## Should Fix

> Things in the Should Fix category are antipatterns and code smells. They 
indicate that something is wrong with the architecture of the code as written. 
A follow-up ticket should be created to restructure things more cleanly.

### Duplicate Code

Sometimes this is just a communication problem; devs don't realize the 
functionality they need already exists, so they re-implement it elsewhere. 
Frequently though, code is duplicated intentionally - often when devs are 
working with unfamiliar code to implement a similar feature. Rather than digging 
through and refactoring the existing code, they'll simply copy and paste what 
they need and then tweak things to make a near-identical function that 
implements the new feature.

Symptoms of this include:
* Copying large blocks of code from one place to another.
* Variable names or comments that reflect the code you copied from rather than the existing functionality.
* Any comments that say "If you update this make sure you also update..."

For more information on Code Duplication and potential fixes, check out the 
[article here](https://sourcemaking.com/refactoring/smells/duplicate-code).

### Over-Generalization

On the opposite end of the spectrum from Duplicate Code is Over-Generalization. 
This happens when you spend too much time trying to future proof your code for 
any features we might want in the future, leading to weird architectural 
complexity. This makes the code needlessly convoluted and hard for other devs to 
understand. In general, the amount of complexity you should have in your 
architecture is the amount you need to accomplish the task at hand. It's easier 
to refactor things to support new use cases once you actually know what those 
use cases are. No matter how much time you spend up front, invariably you'll 
encounter cases in the future that require you to refactor your existing code.

Symptoms of Over-Generalization include:
* Interfaces that are only implemented by a single class, particularly where the 
  class has no methods other than those in the interface.
* Factories that do nothing besides call a constructor for the returned class.
* Class names like IEnumerableObjectFactoryFactory.

To see a examples of Over-Generalization, check out this 
[Enterprise Hello World](https://gist.github.com/lolzballs/2152bc0f31ee0286b722), 
or Rob Ashton's classic 
[You Have Ruined Javascript](http://codeofrob.com/entries/you-have-ruined-javascript.html).

### Data Classes and Extension Classes

Data Classes are classes that contain no methods, just public fields and 
properties. Extension Classes are classes that contain no instance fields or 
properties, but contain a collection of methods for working with some other type 
of object. Both are symptoms of the same problem: not logically grouping methods 
into classes with the data that they operate on. Ideally your classes should 
have both data fields and methods for manipulating those data fields. Deviation 
from that pattern should only happen for a good reason.

Symptoms of this include:
* Classes that contain no methods.
* Classes that have no fields or properties, particularly classes where every 
* method takes the same type of object as a parameter.
* Methods in one class that take an object of a different class as an argument, 
* and then operate solely on that object without referencing their own data.

For more information on Data Classes and their treatment, see the 
[article here](https://sourcemaking.com/refactoring/smells/data-class).

### Shotgun Surgery

"Shotgun Surgery" is an antipattern where code that is structured in such a way 
that relatively trivial changes require modifications to a large number of 
widely-scattered parts of the codebase. This happens when responsibility for 
some action has been split across many classes rather than encapsulated in a 
single one.

Symptoms of Shotgun Surgery include:
* Comments like "When you update this make sure you also update..."
* Guides like "Steps to Add a New X" that list a dozen different classes you 
* need to modify.
* Classes that are so tightly coupled that changes to the internal workings of 
* one require changes to the other one as well.

For more information about the Shotgun Surgery pattern, check out the 
[article](https://sourcemaking.com/refactoring/smells/shotgun-surgery).

### Refused Bequests

Refused Bequests are when a subclass uses only some of the methods and 
properties inherited from its parent classes. It means the inheritance hierarchy 
is skewed. Maybe the refused functionality should more accurately live in a 
different subclass of the parent rather than the parent itself (ADC example: 
CloudThermostats having properties that only make sense for ZWaveThermostats 
because somebody decided to put them in the BaseThermostat parent class). 
Maybe the superclass and the subclass are only tangentially related, and should 
use some other mechanism for sharing functionality.

Symptoms of Refused Bequests:
* Subclasses that have methods or properties that don't make sense for them.
* Classes that inherit methods from their parent classes, but don't ever call 
  those methods.
* Classes that override methods or properties from their parent classes to throw 
  exceptions rather than perform any useful action.

Because you're dying to know more, here's the [article](https://sourcemaking.com/refactoring/smells/refused-bequest).